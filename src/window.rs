use adw::subclass::prelude::*;
use gio::Settings;
use glib::signal::Propagation;
use gtk::prelude::*;
use gtk::{gio, glib, CompositeTemplate};
use std::cell::OnceCell;

use crate::config::APP_ID;
use sharit::widgets::player::SharitPlayer;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/eu/lyes/Sharit/window.ui")]
    pub struct SharitWindow {
        pub settings: OnceCell<Settings>,

        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub player: TemplateChild<SharitPlayer>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SharitWindow {
        const NAME: &'static str = "SharitWindow";
        type Type = super::SharitWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SharitWindow {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();

            obj.setup_settings();
            obj.load_window_size();
        }
    }

    impl WidgetImpl for SharitWindow {}

    impl WindowImpl for SharitWindow {
        fn close_request(&self) -> Propagation {
            self.obj().save_window_size();

            Propagation::Proceed
        }
    }

    impl ApplicationWindowImpl for SharitWindow {}
    impl AdwApplicationWindowImpl for SharitWindow {}
}

glib::wrapper! {
    pub struct SharitWindow(ObjectSubclass<imp::SharitWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl SharitWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::builder().property("application", application).build()
    }

    fn setup_settings(&self) {
        let settings = Settings::new(APP_ID);
        self.imp().settings.set(settings).expect("Unable to set Settings");
    }

    fn settings(&self) -> &Settings {
        self.imp().settings.get().expect("Unable to fetch Settings")
    }

    fn load_window_size(&self) {
        let width = self.settings().int("width");
        let height = self.settings().int("height");
        let maximized = self.settings().boolean("maximized");

        self.set_default_size(width, height);

        if maximized {
            self.maximize();
        } else {
            self.minimize();
        }
    }

    fn save_window_size(&self) {
        let (width, height) = self.default_size();
        let maximized = self.is_maximized();

        self.settings().set_int("width", width).expect("Unable to save width");
        self.settings().set_int("height", height).expect("Unable to save height");
        self.settings().set_boolean("maximized", maximized).expect("Unable to save maximization");
    }
}
