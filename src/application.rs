use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};
use adw::subclass::prelude::*;

use crate::config::VERSION;
use crate::SharitWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct SharitApplication {}

    #[glib::object_subclass]
    impl ObjectSubclass for SharitApplication {
        const NAME: &'static str = "SharitApplication";
        type Type = super::SharitApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for SharitApplication {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();

            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
        }
    }

    impl ApplicationImpl for SharitApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self) {
            let application = self.obj();

            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = SharitWindow::new(application.as_ref());
                window.upcast()
            };
 
            if crate::config::PROFILE == "devel" {
                window.add_css_class(crate::config::PROFILE);
            }

            // Ask the window manager/compositor to present the window
            window.present();
        }

        fn startup(&self) {
            self.parent_startup();
            self.obj().load_css();
        }
    }

    impl GtkApplicationImpl for SharitApplication {}
    impl AdwApplicationImpl for SharitApplication {}
}

glib::wrapper! {
    pub struct SharitApplication(ObjectSubclass<imp::SharitApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl SharitApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::builder().property("application-id", &application_id)
                               .property("flags", flags)
                               .build()
    }

    fn setup_gactions(&self) {
        let quit_action = gio::SimpleAction::new("quit", None);
        quit_action.connect_activate(clone!(@weak self as app => move |_, _| {
            app.quit();
        }));
        self.add_action(&quit_action);

        let about_action = gio::SimpleAction::new("about", None);
        about_action.connect_activate(clone!(@weak self as app => move |_, _| {
            app.show_about();
        }));
        self.add_action(&about_action);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();

        let about = adw::AboutWindow::builder()
            .transient_for(&window)
            .application_name("Sharit")
            .application_icon("eu.lyes.Sharit")
            .developer_name("Lyes Saadi")
            .version(VERSION)
            .developers(vec!["Lyes Saadi"])
            .copyright("© 2023 Lyes Saadi")
            .license_type(gtk::License::MitX11)
            .build();

        about.present();
    }

    fn load_css(&self) {
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/eu/lyes/Sharit/css/style.css");

        gtk::style_context_add_provider_for_display(
            &gtk::gdk::Display::default().expect("Could not connect to a display"),
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION
        );
    }
}
